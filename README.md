# Track the space station in 3d
This project aims to track the  live position of ISS in 3D, using open source libraries such as [WorldWind](https://worldwind.arc.nasa.gov/) and [Satelite.js](https://github.com/shashwatak/satellite-js). All the members of the **Operators** team are actively contributing to the open source project [AcubeSat](https://acubesat.spacedot.gr/) and belong to the OPS (Spacecraft Operations) subsystem. This subsystem is responsible for the development of the Mission Control Software which will receive Telemetry and send Telecommands to the satellite during orbit.

This application is developed under NASA's Space Apps Challenge and more specifically for the "Track the International Space Station in 3D" challenge.

## Building
In order to build, you need to install [NodeJS](https://nodejs.org). The build is known to work with Node.js 12.18.0 LTS.

- Run `npm install` to download WorldWind's dependencies and build everything.

- Run `npm install http-server` to install the server. This is required to actually run the application.

- If you cannot install http-server, try to add `-g` flag to install it globally (it will require sudo permission).

## Running
- `http-server` to run the app which will be hosted at `https:/localhost:8080`.

