//These 2 lines are published by NORAD and allow us to predict where
// the ISS is at any given moment. They are regularly updated.

const ISS_TLE = `1 25544U 98067A   22275.23091245  .00058352  00000+0  10342-2 0  9998	
				2 25544  51.6417 166.2459 0003022 250.0408 254.8118 15.49684437361780` 	//newest tle data

/*const ISS_TLE = 
    `1 25544U 98067A   21121.52590485  .00001448  00000-0  34473-4 0  9997
    2 25544  51.6435 213.5204 0002719 305.2287 173.7124 15.48967392281368`;*/
const satrec = satellite.twoline2satrec(
    ISS_TLE.split('\n')[0].trim(), 
    ISS_TLE.split('\n')[1].trim()
);
/* starting js to create the globe */

var wwd = new WorldWind.WorldWindow("canvasOne"); //this binds the html with id='canvasOne' with the js file

wwd.addLayer(new WorldWind.BMNGOneImageLayer()); //the fallback if online request is not available
//These 2 lines are published by NORAD and allow us to predict where
// the ISS is at any given moment. They are regularly updated.
var config = {dirPath: WorldWind.configuration.baseUrl + 'examples/collada_models/duck/'};
var sat = [];


wwd.addLayer(new WorldWind.BMNGLandsatLayer()); //online image

var modelLayer = new WorldWind.RenderableLayer();
wwd.addLayer(modelLayer);
wwd.addLayer(new WorldWind.CompassLayer());

wwd.addLayer(new WorldWind.CoordinatesDisplayLayer(wwd));

wwd.addLayer(new WorldWind.ViewControlsLayer(wwd));


/* layer is basically layer is the way we display information
it is also used for storing shapes  */

/* ------------ Drawing placemarks ---------------  */

let placemarkLayer = new WorldWind.RenderableLayer("Placemark");
wwd.addLayer(placemarkLayer); //adds to layer



/* -------- COLLADA 3D ----------- */

var modelLayer = new WorldWind.RenderableLayer();
wwd.addLayer(modelLayer);


/* ----------- Accessing a map imagery service ---------  */

var serviceAddress = "https://neo.gsfc.nasa.gov/wms/wms"; //this is the correct site
//"https://neo.sci.gsfc.nasa.gov/wms/wms?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0"; //this site cant be reached

let layerName = "AVHRR_CLIM_W";
/* ---------- possible layerNames -----------
  AVHRR_CLIM_W --> sea surface temperature
  MOD_LSTD_CLIM_M --> surface temperature 
  MODAL2_M_CLD_FR --> cloud fraction 
  MODAL2_M_CLD_OT --> cloud thickness
  AURA_UVI_CLIM_M --> uv index 

   */
let layerArrName = [
	"AVHRR_CLIM_W",
	"MOD_LSTD_CLIM_M",
	"MODAL2_M_CLD_FR",
	"AURA_UVI_CLIM_M",
];

let layerArr = [
	new WorldWind.BMNGLayer(),
	new WorldWind.BingAerialWithLabelsLayer(null),
	(starFieldLayer = new WorldWind.StarFieldLayer()),
	(atmosphereLayer = new WorldWind.AtmosphereLayer()),
];

for (let i = 0; i < layerArr.length; i++) {
	wwd.addLayer(layerArr[i]);
}



/* -------- functions ------------- */

function changeCoordinates() {
	let x = document.forms["coordinates"]["x-axis"].value;
	let y = document.forms["coordinates"]["y-axis"].value;
	// console.log(x);
	// console.log(y);
	if (x == "" || y == "") {
		window.alert("please enter both x and y coordinates");
	} else {
		wwd.navigator.lookAtLocation.latitude = x;
		wwd.navigator.lookAtLocation.longitude = y;
		wwd.navigator.range = 2e6;
		wwd.redraw();
	}
}
/* --- These functions are not finished ----- */
// function hello() {
//   console.log("hello world");

//   layerName = "AVHRR_CLIM_W";

//   layerCreation();
//   console.log(layerName);
//   //wwd.redraw();
// }

// function changeLayerName() {
//   layerName = "MOD_LSTD_CLIM_M";
//   console.log(wwd.get);
//   console.log(layerName);
//   layerCreation();
// }

/* -------- disabling layers  -------- */

function disablingLayers() {
	for (let i = 4; i < layerArr.length; i++) {
		layerArr[i].enabled = false;
	}

	console.log(layerArr);
}

function enablingLayers() {
	for (let i = 4; i < layerArr.length; i++) {
		layerArr[i].enabled = true;
	}

	console.log(layerArr);
}

/* ------------ Picking imageryService -------------- */

function pickImageryService() {
	var select = document.getElementById("service");
	var value = select.options[select.selectedIndex].value;

	layerName = value;
	layerCreation();
}

function layerCreation() {
	var createLayer = function (xmlDom) {
		var wms = new WorldWind.WmsCapabilities(xmlDom);
		var wmsLayerCapabilities = wms.getNamedLayer(layerName);
		var wmsConfig =
			WorldWind.WmsLayer.formLayerConfiguration(wmsLayerCapabilities);
		var wmsLayer = new WorldWind.WmsLayer(wmsConfig);

		wwd.addLayer(wmsLayer);
		layerArr.push(wmsLayer);
		console.log(layerArr);
	};

	var logError = function (jqXhr, text, exception) {
		console.log(
			"There was a failure retrieving the capabilities document: " +
				text +
				" exception: " +
				exception
		);
	};

	$.get(serviceAddress).done(createLayer).fail(logError);
}

/* -------------- Placing placemarks ------------------ */

function createPlacemark() {
	let x = Number(document.forms["placemark"]["x-axis"].value);
	let y = Number(document.forms["placemark"]["y-axis"].value);
	console.log(typeof y);
	let location = document.forms["placemark"]["name"].value;
	if (location == "") {
		window.alert("please enter a valid name");
	} else {
		let placemarkAttributes = new WorldWind.PlacemarkAttributes(null);

		placemarkAttributes.imageOffset = new WorldWind.Offset(
			WorldWind.OFFSET_FRACTION,
			0.3,
			WorldWind.OFFSET_FRACTION,
			0.0
		); //changes the position of the pin

		placemarkAttributes.labelAttributes.color = WorldWind.Color.WHITE; // changes the color of the pin
		placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
			WorldWind.OFFSET_FRACTION,
			0.5,
			WorldWind.OFFSET_FRACTION,
			1.0
		); //changes the position of the text

		placemarkAttributes.imageSource =
			WorldWind.configuration.baseUrl + "images/pushpins/plain-red.png";

		var position = new WorldWind.Position(x, y, 100.0); // x y z coordinates of the mark
		var placemark1 = new WorldWind.Placemark(
			position,
			false,
			placemarkAttributes
		);

		placemark1.label =
			location +
			"\n" +
			"Lat " +
			placemark1.position.latitude.toPrecision(4).toString() +
			"\n" +
			"Lon " +
			placemark1.position.longitude.toPrecision(5).toString(); //what the marker says
		placemark1.alwaysOnTop = true; //if its over other shapes
		placemarks.push(placemark1);
		placemarkLayer.addRenderable(placemark1); //adds to globe
		console.log(placemarks);
	}
}

placemarks = [];

function disableplacemarks() {
	console.log(placemarks);
	for (let i = 0; i < placemarks.length; i++) {
		placemarks[i].enabled = false;
	}
}

function enableplacemarks() {
	console.log(placemarks);
	for (let i = 0; i < placemarks.length; i++) {
		placemarks[i].enabled = true;
	}
}

/* ---------- creating shapes ----------------- */

shapes = [];

function shapeCreation() {
	let x = [];
	let y = [];
	x.push(Number(document.forms["pin1"]["x-axis"].value));
	x.push(Number(document.forms["pin2"]["x-axis"].value));
	x.push(Number(document.forms["pin3"]["x-axis"].value));
	y.push(Number(document.forms["pin1"]["y-axis"].value));
	y.push(Number(document.forms["pin2"]["y-axis"].value));
	y.push(Number(document.forms["pin3"]["y-axis"].value));
	console.log(x);
	console.log(y);

	var polygonAttributes = new WorldWind.ShapeAttributes(null); // dont know about null
	polygonAttributes.interiorColor = new WorldWind.Color(0, 1, 1, 0.75); // RGBA where a --> opacity
	polygonAttributes.outlineColor = WorldWind.Color.RED; // the periphery of the shape
	polygonAttributes.drawOutline = true; // to draw the outline
	polygonAttributes.applyLighting = true; // for shading

	/* the boundaries of our polygon
      in our case we will create a triangle 
      */
	var boundaries = [];
	boundaries.push(new WorldWind.Position(x[0], y[0], 700000.0)); // x y z
	boundaries.push(new WorldWind.Position(x[1], y[1], 700000.0));
	boundaries.push(new WorldWind.Position(x[2], y[2], 700000.0));
	//boundaries.push(new WorldWind.Position(14.0, -76.0, 500000.0));

	/* to construct the polygon with the aformentioned boundaries */

	var polygon = new WorldWind.Polygon(boundaries, polygonAttributes);
	polygon.extrude = true; // to display it out of the earth
	// if false we would see a triangle without the thickness
	// basically a hovering triangle
	shapes.push(polygon);
	console.log(shapes);
	polygonLayer.addRenderable(polygon);
}

function disableShapes() {
	for (let i = 0; i < shapes.length; i++) {
		shapes[i].enabled = false;
	}
}

function enableShapes() {
	for (let i = 0; i < shapes.length; i++) {
		shapes[i].enabled = true;
	}
}

/* ------------ add Duckies -------------- */

duckies = [];

function createDuckies() {
	let x = Number(document.forms["duckie"]["x-axis"].value);
	let y = Number(document.forms["duckie"]["y-axis"].value);
	console.log(x, y);
	var position = new WorldWind.Position(x, y, 60000.0); // latitude, longitude, and altitude
	var config = {
		dirPath: WorldWind.configuration.baseUrl + "examples/collada_models/duck/",
	}; //i assume this is the path fot the duck to load

	var colladaLoader = new WorldWind.ColladaLoader(position, config);

	/* 1st param : the file we want 
    2nd param: a callback funct which works async and loads the model and the scale */

	colladaLoader.load("duck.dae", function (colladaModel) {
		//"duck.dae"
		duckies.push(colladaModel);
		colladaModel.scale = 3000;
		modelLayer.addRenderable(colladaModel);
		console.log(colladaModel);
	});
}

function disableDuckies() {
	for (let i = 0; i < duckies.length; i++) {
		duckies[i].enabled = false;
	}
}

function enableDuckies() {
	for (let i = 0; i < duckies.length; i++) {
		duckies[i].enabled = true;
	}
}

/* ---------- star field ----------- */

// Set a date property for the StarField and Atmosphere layers to the current date and time.
// This enables the Atmosphere layer to show a night side (and dusk/dawn effects in Earth's terminator).
// The StarField layer positions its stars according to this date.
var now = new Date();
starFieldLayer.time = now;
atmosphereLayer.time = now;

// In this example, each full day/night cycle lasts 8 seconds in real time.
var simulatedMillisPerDay = 24 * 3600 * 1000;

// Begin the simulation at the current time as provided by the browser.
var startTimeMillis = Date.now();

function runSimulation() {
	// Compute the number of simulated days (or fractions of a day) since the simulation began.
	var elapsedTimeMillis = Date.now() - startTimeMillis;
	var simulatedDays = elapsedTimeMillis / simulatedMillisPerDay;

	// Compute a real date in the future given the simulated number of days.
	var millisPerDay = 24 * 3600 * 1000; // 24 hours/day * 3600 seconds/hour * 1000 milliseconds/second
	var simulatedMillis = simulatedDays * millisPerDay;
	var simulatedDate = new Date(startTimeMillis + simulatedMillis);

	// Update the date in both the Starfield and the Atmosphere layers.
	starFieldLayer.time = simulatedDate;
	atmosphereLayer.time = simulatedDate;
	wwd.redraw(); // Update the WorldWindow scene.

	requestAnimationFrame(runSimulation);
}

// Animate the starry sky as well as the globe's day/night cycle.
requestAnimationFrame(runSimulation);

/* ------------ check if you can see the ISS --------- */
function createCircle() {
	let x = Number(document.forms["placemark"]["x-axis"].value);
	let y = Number(document.forms["placemark"]["y-axis"].value);
	var circleAttributes = new WorldWind.ShapeAttributes(null); // dont know about null
	circleAttributes.interiorColor = new WorldWind.Color(0, 1, 1, 0.75); // RGBA where a --> opacity
	circleAttributes.outlineColor = WorldWind.Color.RED; // the periphery of the shape
	circleAttributes.drawOutline = true; // to draw the outline
	circleAttributes.applyLighting = true; // for shading
	var circle = new WorldWind.SurfaceCircle(
		new WorldWind.Location(x, y),
		2750e3,
		circleAttributes
	);

	// var polygon = new WorldWind.Polygon(boundaries, circleAttributes);
	circle.extrude = true; // to display it out of the earth
	placemarks.push(circle);
	placemarkLayer.addRenderable(circle);
}

var start= Cesium.JulianDate.fromDate(new Date());
var count=0;
var latitude = 0;
var longitude = 0;
var altitude = 0;
//var sec =0;
var date = new Date();
var modelLayerOrbit = new WorldWind.RenderableLayer();
wwd.addLayer(modelLayerOrbit);
//Create predicted orbit
setInterval(function(){
	/*var time = Cesium.JulianDate.addSeconds(start, sec, new Cesium.JulianDate());
    var jsDate = Cesium.JulianDate.toDate(time);*/
    
    var positionAndVelocity = satellite.propagate(satrec, date);
    var gmst = satellite.gstime(date);
    var p   = satellite.eciToGeodetic(positionAndVelocity.position, gmst);
	console.log(gmst);
	console.log(p);
    //var position = Cesium.Cartesian3.fromRadians(p.longitude, p.latitude, 45 * 1000);
    var Windposition = new WorldWind.Position(p.latitude,p.longitude,450000.0);


	var colladaLoaderdot = new WorldWind.ColladaLoader(Windposition, ISSconfig);
        colladaLoaderdot.load("sphere.dae",function(colladaModel2){
            colladaModel2.scale = 800;
            modelLayerOrbit.addRenderable(colladaModel2);
        });
	date.setMinutes(date.getMinutes() + 15);
    //sec += 0.0001;
},1000);

var count1 = 0;
var latitude1 = 0;
var longitude1 = 0;
var altitude1 = 0;

var URL = 'https://api.wheretheiss.at/v1/satellites/25544'; //live 3D api that's having server errors
var URL2= 'http://api.open-notify.org/iss-now.json'		//live 2D api that's being used with the constant ISS altitude 
var ISSconfig = {dirPath: /3dModels/};
setInterval(async function(){
	//	3D api position
	var width = window.innerWidth;
	var height = window.innerHeight;
	document.getElementById("canvasOne").style.width = width + "px";
	document.getElementById("canvasOne").style.height = height + "px";
	var response = await axios.get(URL);
	latitude1 = response.data.latitude;			
	longitude1 = response.data.longitude; 
	altitude1 = response.data.altitude;

	//	2D api position
    /*var response = await axios.get(URL2)
    latitude1=response.data.iss_position.latitude;
    longitude1=response.data.iss_position.longitude;*/
	document.getElementById("longitude-el").innerHTML = "Longitude: " + longitude1;
	document.getElementById("latitude-el").innerHTML = "Latitude: " + latitude1;
	document.getElementById("altitude-el").innerHTML = "Altitude: " + altitude1*1000 + "m";
    var Windposition = new WorldWind.Position(latitude1, longitude1, altitude1*1000);	
    var colladaLoader= new WorldWind.ColladaLoader(Windposition,ISSconfig);
    colladaLoader.load("ISS.dae",function(colladaModel){
        colladaModel.scale=2000000;
        modelLayer.addRenderable(colladaModel);
        sat.push(colladaModel)
        // Remove previous entry
        if(count1 > 0) modelLayer.removeRenderable(sat[count1-1]);
        count1++;
    })

    var colladaLoaderdot = new WorldWind.ColladaLoader(Windposition, ISSconfig);
        colladaLoaderdot.load("sphere.dae",function(colladaModel1){
            colladaModel1.scale = 200;
            modelLayer.addRenderable(colladaModel1);
        });
}, 1000);

function togglePredictedOrbit(){
	modelLayerOrbit.enabled = !modelLayerOrbit.enabled;
}
